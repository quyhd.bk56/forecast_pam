from builtins import object

from keras import Sequential
from keras.layers import LSTM, Dense
from matplotlib import pyplot
from sklearn.preprocessing import MinMaxScaler


# noinspection PyMethodMayBeStatic
class utils(object):
    def __init__(self, coin=None, n_hours=1, n_time_predicts=1, config_train=None, units=None, opt='adam'):
        self.coin = coin
        self.n_hours = n_hours
        self.n_time_predicts = n_time_predicts
        self.config_train = config_train
        self.units = units
        self.opt = opt
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        # self.db = ConnectDB()
        # self.scaler = MinMaxScaler(feature_range=(0, 1))

    def build_model(self, units, train_X, loss='mse', optimizer='adam'):
        model = Sequential()
        model.add(LSTM(units, input_shape=(train_X.shape[1], train_X.shape[2]), return_sequences=True))
        model.add(LSTM(units, return_sequences=True))
        model.add(LSTM(units))
        model.add(Dense(self.n_hours, activation='relu'))
        model.summary()
        model.compile(loss=loss, optimizer=optimizer)
        return model

    def fit_model(self, model, train_X, train_y, test_X, test_y, config):
        epochs, batch_size, verbose, min_delta, patience, monitor = config
        history = model.fit(train_X, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose, shuffle=False, validation_data=(test_X, test_y))
        pyplot.plot(history.history['loss'], label='train')
        pyplot.plot(history.history['val_loss'], label='test')
        pyplot.legend()
        pyplot.show()
        return model

    def save_model(self, model, symbol):
        model.save_weights("weights/weight_%s.h5" % symbol)
        model_json = model.to_json()
        with open("models/model_%s.json" % symbol, "w") as json_file:
            json_file.write(model_json)
