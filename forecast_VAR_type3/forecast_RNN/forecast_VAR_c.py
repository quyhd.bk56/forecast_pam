import csv
import datetime
import json
import os
import numpy
import pandas as pd
from matplotlib import pyplot
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.vector_ar.var_model import VAR


# noinspection PyShadowingNames
def recursive_(init_step, predict_t, init_train, cols):
    if init_step > 0:
        model = VAR(endog=init_train)
        model_fit = model.fit()
        yhat = model_fit.forecast(model_fit.y, steps=1)
        init_step = init_step - 1
        init_train_v = init_train.values
        new_train = numpy.concatenate([init_train_v, yhat])
        df = pd.DataFrame(new_train, columns=cols)
        recursive_(init_step, predict_t, df, cols)
    else:
        o = init_train.values[len(init_train) - predict_t:, 0:1]
        return o


config = {}
ROOT_DIR = os.path.dirname(os.path.dirname(__file__))
config_path = os.path.join(ROOT_DIR, 'forecast_RNN', 'config' + '.json')
with open(config_path, 'r') as myfile:
    if len(myfile.readlines()) != 0:
        myfile.seek(0)
        config = json.load(myfile)
# ======================================================================================================================
weather_v_filepath = os.path.join(ROOT_DIR, 'data_csv', 'darkinfohourly' + '.csv')
csv_reader = pd.read_csv(weather_v_filepath)
out_w = csv_reader.filter(
    ["timezone", "data.time", "data.summary", "data.windSpeed", "data.windBearing", "code", "longtime"])
out_w = out_w[out_w['code'] == 2]
# ======================================================================================================================
weather_l = []
weather_path = os.path.join(ROOT_DIR, 'package_func', 'weather_def' + '.json')
weather_def_l = []
with open(weather_path, 'r', encoding='utf-8') as myfile:
    if len(myfile.readlines()) != 0:
        myfile.seek(0)
        weather_def_l = json.load(myfile)
for x in out_w.values:
    mem_w = []
    mem_w_ws = x[3]
    mem_w_wb = x[4]
    mem_w_time = datetime.datetime.fromtimestamp(x[6]).strftime('%d/%m/%Y %H:%M:%S')
    mem_w_h = datetime.datetime.fromtimestamp(x[6]).hour
    mem_w_w = next((y for y in weather_def_l if y['en'] == x[2]), None)['index']
    mem_w.append(mem_w_time)
    mem_w.append(mem_w_h)
    mem_w.append(mem_w_w)
    mem_w.append(mem_w_ws)
    mem_w.append(mem_w_wb)
    weather_l.append(mem_w)
# ======================================================================================================================
verbose = 1
min_delta = 1e-30
patience = 30
monitor = 'val_loss'
coin = None
demo = True

# config_train = (config["epochs"], config["batch_size"], verbose, min_delta, patience, monitor)
if config["isscale"] == 1:
    isscale = True
else:
    isscale = False
# ======================================================================================================================
files_l = []
for files in config["files"]:
    file_path = os.path.join(ROOT_DIR, 'data_csv', files)
    # data = pd.read_csv(file_path, delimiter=',', header=0, skipinitialspace=True)
    dfs = pd.read_excel(file_path, sheet_name=None)
    dfs_numpy = dfs['data']
    files_l.append(dfs_numpy.values.tolist())

# data.head(24)
index_time = []
for i in files_l:
    for j in i:
        index_time.append(j[0])
for i in weather_l:
    index_time.append(i[0])
index_time_set = list(set(index_time))
index_time_s = sorted(index_time_set, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y %H:%M:%S'))
merge_l = []
for i in index_time_s:
    mem = []
    dt = datetime.datetime.strptime(i, '%d/%m/%Y %H:%M:%S').hour
    mem.append(i)
    mem.append(dt)
    weather_c = next((y for y in weather_l if y[0] == i), None)
    if weather_c is not None:
        mem.append(weather_c[2])
        mem.append(weather_c[3])
        mem.append(weather_c[4])
    else:
        mem.append("nan")
    for f1 in files_l:
        first_or_default = next((x for x in f1 if x[0] == i), None)
        if first_or_default is not None:
            mem.append(first_or_default[1])
        else:
            mem.append("nan")
    if "nan" not in mem:
        merge_l.append(mem)
# ======================================================================================================================
notonly = True
# horizontally stack columns
dataset_ = None
if notonly:
    a = numpy.array(merge_l)
    dataset_ = a[:, 1:config["n_features"] + 2].astype(numpy.float)
else:
    pass
a_ = a[:, 1:config["n_features"] + 2].astype(numpy.float)
a__ = numpy.append(a[:, 0:1], a_, axis=1)

dataset_2 = pd.DataFrame(a__)
dataset_2 = dataset_2.set_index(0)

for i in range(0, len(dataset_2.columns)):
    dataset_2[i + 1] = dataset_2[i + 1].astype('float64')

t = dataset_2.diff().dropna()
t.describe()
# ======================================================================================================================
train = t[:len(t) - config["n_hours"]]
valid = t[len(t) - config["n_hours"]:]

mod = VAR(train)
res = mod.fit(maxlags=config["n_hours"], ic='aic')

predic = res.forecast(res.y, config["n_hours"])
pred_df = pd.DataFrame(predic)
pred_inverse = pred_df.cumsum()
temp_v = dataset_2[len(dataset_2) - config["n_hours"]:].reset_index().drop(columns=[0])
f = pred_inverse + temp_v
# ======================================================================================================================

pyplot.figure(figsize=(12, 5))
pyplot.xlabel('Index')
ax1 = temp_v[config['n_features'] - 1].plot(color='blue', grid=True, label='Actual')
ax2 = f[config['n_features'] - 1].plot(color='red', grid=True, secondary_y=True, label='Predict')
ax1.legend(loc=1)
ax2.legend(loc=1)

folder_save_png = os.path.join(ROOT_DIR, 'img', 'chart' + datetime.datetime.now().strftime("%m%d%Y%H%M%S") + '.png')
pyplot.savefig(folder_save_png)
pyplot.show()
pyplot.close()

o_out = temp_v[config['n_features'] - 1].values.reshape(config["n_hours"], 1)
f_out = f[config['n_features'] - 1].values.reshape(config["n_hours"], 1)
out_list = numpy.hstack((o_out, f_out)).tolist()
out_csv = os.path.join(ROOT_DIR, 'data_csv', 'data_var2_' + datetime.datetime.now().strftime("%m%d%Y%H%M%S") + '.csv')
with open(out_csv, "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(out_list)

print("""""""")
