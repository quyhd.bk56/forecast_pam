from math import sqrt

from keras import Sequential
from keras.layers import LSTM, Dropout, Dense

import numpy as np
from numpy import concatenate
from pandas import read_csv, DataFrame, concat

from matplotlib import pyplot
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler, LabelEncoder


def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# load dataset
dataset = read_csv(r'D:\MyApp\PRJ\tensorflow_RNN\data_csv\pollution.csv', header=0, index_col=0)
values_ = dataset.values

groups = [4, 5, 6, 7, 9, 10, 11]
n_hours = 24
n_train_hours = 365 * 24 * 2
# n_time_predicts = 7 * 24
epochs = 100
batch_size = 72
verbose = 2
n_features = 7

raw_values = np.delete(values_, 8, 1)
# integer encode direction
encoder = LabelEncoder()
raw_values[:, 4] = encoder.fit_transform(raw_values[:, 4])
# ensure all data is float
values = raw_values.astype('float32')
time_s, values = values[:, :4], values[:, 4:]

# frame as supervised learning
reframed = series_to_supervised(values, 1, n_hours)
index = 0
for i in range(0, n_hours):
    # drop columns we don't want to predict
    # 8 15 22
    # 8 9
    reframed.drop(reframed.columns[[index + 8, index + 9, index + 10, index + 11, index + 12, index + 13]], axis=1, inplace=True)
    index = index + 1
print(reframed.head())

# split into train and test sets
values_reframe = reframed.values
# normalize features
scaler = MinMaxScaler(feature_range=(0, 1))
scaled = scaler.fit_transform(values_reframe)
values_scaled = scaled
train = values_scaled[:n_train_hours, :]
test = values_scaled[n_train_hours:, :]
# split into input and outputs
train_X, train_y = train[:, :n_features], train[:, n_features:train.shape[1]]
test_X, test_y = test[:, :n_features], test[:, n_features:train.shape[1]]
# reshape input to be 3D [samples, timesteps, features]
train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
test_X = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))
print(train_X.shape, train_y.shape, test_X.shape, test_y.shape)

# design network
model = Sequential()
model.add(LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2])))
model.add(Dense(n_hours))
model.compile(loss='mae', optimizer='adam')
# fit network
history = model.fit(train_X, train_y, epochs=epochs, batch_size=batch_size, validation_data=(test_X, test_y), verbose=verbose,
                    shuffle=False)
# plot history
pyplot.plot(history.history['loss'], label='train')
pyplot.plot(history.history['val_loss'], label='test')
pyplot.legend()
pyplot.show()

# make a prediction
yhat = model.predict(test_X)
test_X = test_X.reshape((test_X.shape[0], test_X.shape[2]))
# invert scaling for forecast
# inv_yhat = concatenate((yhat, test_X[:, :-1]), axis=1)
inv_yhat = concatenate((test_X[:, :], yhat), axis=1)
inv_yhat_ = scaler.inverse_transform(inv_yhat)
inv_yhat = inv_yhat_[:, n_features:n_features + 1]
# invert scaling for actual
test_y = test_y.reshape((len(test_y), test_y.shape[1]))
inv_y = concatenate((test_X[:, :], test_y), axis=1)
inv_y_ = scaler.inverse_transform(inv_y)
inv_y = inv_y_[:, n_features:n_features + 1]
# calculate RMSE
rmse = sqrt(mean_squared_error(inv_y, inv_yhat))
print('Test RMSE: %.3f' % rmse)

print("=================================")

pyplot.title("Chart Actual", fontsize=14)
pyplot.plot(inv_y, color='violet', label='Actual')
pyplot.show()
pyplot.title("Chart Forecast", fontsize=14)
pyplot.plot(inv_yhat, color='g', label='Forecast')
pyplot.show()

pyplot.legend(loc="upper left")
pyplot.xlabel("Time Periods")

pyplot.close()

