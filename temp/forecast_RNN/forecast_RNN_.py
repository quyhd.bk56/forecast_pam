import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# file_path = 'weather.csv'
file_path = 'Bát Tràng_averagegio.csv'
data = pd.read_csv(file_path, delimiter=',', header=0, skipinitialspace=True)
data.head(24)

temperature = np.array(data['Value'])
temperature = temperature[6:]
num_periods = 24 * 3
f_horizon = 1  # features
time_forecast = num_periods
x_data = temperature[:(len(temperature) - time_forecast)]
# x_batches la list array, moi array con trong list la 1 bo 24 value
# convert x_data sang list array , moi array co num_periods phan tu
# y_batches la list array forecast sau do 1h
# x_batches va y_batches de tranning model
x_batches = x_data.reshape(-1, num_periods, 1)

y_data = temperature[1:(len(temperature) - time_forecast) + f_horizon]
y_batches = y_data.reshape(-1, num_periods, 1)
print(y_batches.shape)


# lay tam 2 X_test , Y_test la du lieu 48h gan nhat - 1 va du lieu 48h gan nhat => de test
def test_data(series, forecast, num):
    testX = series[-(num + forecast):][:num].reshape(-1, num_periods, 1)
    testY = series[-num:].reshape(-1, num_periods, 1)
    return testX, testY


X_test, Y_test = test_data(temperature, f_horizon, time_forecast)
print(X_test.shape)

# build AI model
tf.reset_default_graph()
inputs = 1
rnn_size = 100
output = 1
learning_rate = 0.001
dropout_keep_prob = tf.placeholder(tf.float32)

X = tf.placeholder(tf.float32, [None, num_periods, 1])
Y = tf.placeholder(tf.float32, [None, num_periods, 1])

rnn_cells = tf.contrib.rnn.BasicRNNCell(num_units=rnn_size, activation=tf.nn.relu)
# tf.nn.rnn_cell.RNNCell()
rnn_output, states = tf.nn.dynamic_rnn(rnn_cells, X, dtype=tf.float32)

output = tf.reshape(rnn_output, [-1, rnn_size])
logit = tf.layers.dense(output, 1, name="softmax")

outputs = tf.reshape(logit, [-1, num_periods, 1])
print(logit)

loss = tf.reduce_sum(tf.square(outputs - Y))

accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logit, 1), tf.cast(Y, tf.int64)), tf.float32))

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_step = optimizer.minimize(loss)

# tranning model
epochs = 1000

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)
saver = tf.train.Saver()

for epoch in range(epochs):
    train_dict = {X: x_batches, Y: y_batches, dropout_keep_prob: 0.5}
    sess.run(train_step, feed_dict=train_dict)

save_path = saver.save(sess, "models/weather.ckpt")

# restore model
with tf.Session() as sess:
    saver.restore(sess, "models/weather.ckpt")
    # y_pred=sess.run(outputs, feed_dict={X: X_test})
    # print (y_pred)

    # forecast sau khi tranning
    y_pred = sess.run(outputs, feed_dict={X: X_test})

# printf so sanh
plt.title("Forecast vs Actual", fontsize=14)
plt.plot(pd.Series(np.ravel(Y_test)), color='g', label="Actual")
plt.plot(pd.Series(np.ravel(y_pred)), color='orange', label="Forecast")
plt.legend(loc="upper left")
plt.xlabel("Time Periods")
plt.show()
