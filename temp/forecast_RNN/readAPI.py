import json

import requests


def read_pamair(fromdate, todate, localId):
    data_temp = {
        "method": "post",
        "param":
            {
                "localId": localId,
                "enviobjectid": "",  # temp
                "fromdate": fromdate,
                "todate": todate,
                "calculator": "average1gio"
            }
    }

    data_hum = {
        "method": "post",
        "param":
            {
                "localId": localId,
                "enviobjectid": "_2_0_SHT1X",  # hum
                "fromdate": fromdate,
                "todate": todate,
                "calculator": "average1gio"
            }
    }

    data_pm25 = {
        "method": "post",
        "param":
            {
                "localId": localId,
                "enviobjectid": "_1_1_PMX",  # pm25
                "fromdate": fromdate,
                "todate": todate,
                "calculator": "average1gio"
            }
    }

    headers = {'Content-type': 'application/json'}
    r_temp = requests.post("https://pamair.org/pamenviad/sub/ref/109", json=data_temp, headers=headers)
    r_hum = requests.post("https://pamair.org/pamenviad/sub/ref/109", json=data_hum, headers=headers)
    r_pm25 = requests.post("https://pamair.org/pamenviad/sub/ref/109", json=data_pm25, headers=headers)
    # noinspection PyListCreation
    list_v = []
    list_v.append(r_temp.json())
    list_v.append(r_hum.json())
    list_v.append(r_pm25.json())

    return list_v
