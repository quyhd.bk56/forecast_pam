from pandas import DataFrame, concat
import pandas as pd
import numpy as np
from numpy import array
from matplotlib import pyplot
from sklearn.preprocessing import MinMaxScaler

# noinspection PyMethodMayBeStatic,PyShadowingNames
from forecast_RNN_type3.package_func import utils


def invert_scaling(test_y, test_X_reshape, part_2, n_features, isscale, demo):
    first_y = np.concatenate((test_y[:, 0], test_y[-1:, 1:]), axis=None)
    first_y_reshape = first_y.reshape(first_y.shape[0], 1)
    test_X_reshape_split = test_X_reshape[:, :(n_features - 1)]
    # new_data_forecast =
    # inv_y = np.concatenate((test_y, test_X_reshape[:, -(n_features - 1):]), axis=1)

    # inv_y = np.concatenate((first_y_reshape, test_X_reshape_split), axis=1)
    if demo:
        inv_y_part = np.concatenate((test_X_reshape_split, test_y[:, 0].reshape(test_y.shape[0], 1)), axis=1)
    else:
        inv_y_part = np.concatenate((part_2[:, :(n_features - 1)], test_y[test_y.shape[0] - 1].reshape(n_hours, 1)),
                                    axis=1)
    # inv_y = self.scaler.inverse_transform(inv_y)
    # inv_y = inv_y[:, 0]
    return inv_y_part


class trainModel(object):
    # split a multivariate sequence into samples
    # noinspection PyShadowingNames
    def __init__(self, coin=None, n_hours=1, n_time_predicts=1, config_train=None, units=None, opt='adam'):
        self.coin = coin
        self.n_hours = n_hours
        self.n_time_predicts = n_time_predicts
        self.config_train = config_train
        self.units = units
        self.opt = opt
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        # self.db = ConnectDB()
        # self.scaler = MinMaxScaler(feature_range=(0, 1))

    # noinspection PyMethodMayBeStatic,PyShadowingNames
    def split_sequence_one(self, sequence, n_steps_in, n_steps_out):
        X, y = list(), list()
        for i in range(len(sequence)):
            # find the end of this pattern
            end_ix = i + n_steps_in
            out_end_ix = end_ix + n_steps_out
            # check if we are beyond the sequence
            if out_end_ix > len(sequence):
                break
            # gather input and output parts of the pattern
            seq_x, seq_y = sequence[i:end_ix], sequence[end_ix:out_end_ix]
            X.append(seq_x)
            y.append(seq_y)
        return array(X), array(y)

    # split a multivariate sequence into samples
    # noinspection PyMethodMayBeStatic
    def split_sequences(self, sequences, n_steps_in, n_steps_out):
        X, y = list(), list()
        for i in range(len(sequences)):
            # find the end of this pattern
            end_ix = i + n_steps_in
            out_end_ix = end_ix + n_steps_out - 1
            # check if we are beyond the dataset
            if out_end_ix > len(sequences):
                break
            # gather input and output parts of the pattern
            seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix - 1:out_end_ix, -1]
            X.append(seq_x)
            y.append(seq_y)
        return array(X), array(y)

    # noinspection PyMethodMayBeStatic,PyShadowingNames
    def series_to_supervised(self, data, n_out=1, n_in=1, dropnan=True):
        n_vars = 1 if type(data) is list else data.shape[1]
        df = DataFrame(data)
        # df_drop = DataFrame(data)
        # df_drop.drop(df_drop.columns[range(1, n_vars)], axis=1, inplace=True)
        cols, names = list(), list()
        # input sequence (t-n, ... t-1)
        for i in range(n_in, 0, -1):
            cols.append(df.shift(i))
            # if i == 0:
            #     names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
            # else:
            names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
        # forecast sequence (t, t+1, ... t+n)
        for i in range(0, n_out):
            # cols.append(df_drop.shift(-i))
            cols.append(df.shift(-i))
            if i == 0:
                names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
            else:
                names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
        agg = concat(cols, axis=1)
        agg.columns = names
        # drop rows with NaN values
        if dropnan:
            agg.dropna(inplace=True)
        agg = agg.fillna(0)
        return agg

    # noinspection PyShadowingNames
    def normalize_data(self, dataset, dropnan=True):
        values = dataset.values
        values = values.astype('float32')
        reframed = self.series_to_supervised(values, self.n_hours, 1, dropnan)
        values = reframed.values
        return values

    # noinspection PyShadowingNames,PyMethodMayBeStatic
    def modifire_dataset(self, dataset, n_hours, n_features):
        X, Y = list(), list()
        for i in range(len(dataset)):
            if i >= len(dataset) - n_hours:
                continue
            start_i = i
            end_i = i + n_hours
            if n_features == 1:
                gr_data = dataset[start_i + 1:end_i + 1]
            else:
                gr_data = dataset[start_i + 1:end_i + 1, -1:]
            local_data = dataset[i]
            numpy_add = np.append(local_data, gr_data)
            X.append(numpy_add)
            Y.append(numpy_add.tolist())
        # X = array(X)
        # X = X.reshape((X.shape[0], X.shape[1], n_features))
        return X, Y

    # noinspection PyMethodMayBeStatic,PyShadowingNames
    def split_train_test(self, values, n_time_predicts):
        n_train_hours = len(values) - n_time_predicts
        # train = values[:n_train_hours,
        #         :]  # lấy hàng 0 đến hàng n_train_hours và tất cả các cột của mảng đa chiều values
        # test = values[n_train_hours:, :]  # lấy hàng n_train_hours đến hết  và tất cả các cột của mảng đa chiều values

        train, valid = np.split(values, [int(0.7 * len(values))])
        return train, valid

    # noinspection PyMethodMayBeStatic,PyShadowingNames,PyShadowingNames
    def split_into_inputs_and_outputs(self, values, n_features=10, n_time_predicts=5, n_hours=2):
        # n_time_predicts = len(values)
        n_obs = n_time_predicts * n_features
        X, y = values[:, :n_obs], values[:, n_obs:values.shape[1]]

        # if notonly:
        #     X = X.reshape((X.shape[0], X.shape[1], n_features))
        # else:
        #     X = X.reshape((n_time_predicts, 1, n_features))

        # X = X.reshape((n_time_predicts, 1, n_features))
        return X, y

    # noinspection PyUnusedLocal,PyShadowingNames,PyShadowingNames,PyMethodMayBeStatic
    def make_predict(self, model, test_X, part_2, n_features=1, isscale=True, demo=True):
        yhat = model.predict(test_X)
        test_X = test_X.reshape((test_X.shape[0], test_X.shape[1] * n_features))
        inv_yhat_part = invert_scaling(yhat, test_X, part_2, n_features, isscale, demo)
        return inv_yhat_part

    # noinspection PyMethodMayBeStatic,PyShadowingNames
    def save_img_predict_test(self, test_y, inv_yhat, symbol):
        print("=================================")
        pyplot.title("Chart", fontsize=14)
        pyplot.plot(test_y, color='violet', label='Actual')
        # pyplot.legend(loc="upper left")
        #         # pyplot.xlabel("Time Periods")
        #         # pyplot.show()
        #         # pyplot.title("Forecast", fontsize=14)
        pyplot.plot(inv_yhat, color='g', label='Forecast')
        pyplot.legend(loc="upper left")
        pyplot.xlabel("Time Periods")
        pyplot.show()

        pyplot.savefig("img/chart_%s.png" % symbol)
        pyplot.close()

    # noinspection PyTypeChecker,PyUnusedLocal,PyShadowingNames
    def train_model(self, X, y, X_test, y_test, dataset_scaling, n_features, isscale, demo):
        model = utils.build_model(units=self.units, train_X=X, optimizer=opt)
        model = utils.fit_model(model, X, y, X_test, y_test, self.config_train)
        utils.save_model(model, "pm25")
        # other_part = dataset_scaling[(dataset_scaling.shape[0] - self.n_hours):, :(n_features - 1)]
        part_2 = dataset_scaling[(dataset_scaling.shape[0] - self.n_hours):, :]
        part_1 = dataset_scaling[:(dataset_scaling.shape[0] - self.n_hours), :]
        if demo:
            # ==============================================================================================================
            yhat_part = self.make_predict(model, X, X, n_features, isscale, True)
            # predict_out_all = np.concatenate((part_1, yhat_part))
            predict_out_all = yhat_part
            if isscale:
                inv_predict_out = self.scaler.inverse_transform(predict_out_all)
            else:
                inv_predict_out = predict_out_all
            # inv_yhat = np.concatenate((yhat[:, 0], yhat[-1:, 1:]), axis=None)
            inv_yhat = inv_predict_out[:, (n_features - 1):]
            if isscale:
                o_dataset = self.scaler.inverse_transform(X.reshape((X.shape[0], 1 * n_features)))
            else:
                o_dataset = dataset_scaling
            inv_test_y = o_dataset[:, (n_features - 1):]
        else:
            pass
        # ==============================================================================================================
        # yhat_part = self.make_predict(model, X_test, part_2, n_features, isscale)
        # predict_out_all = np.concatenate((part_1, yhat_part))
        # if isscale:
        #     inv_predict_out = self.scaler.inverse_transform(predict_out_all)
        # else:
        #     inv_predict_out = predict_out_all
        # # inv_yhat = np.concatenate((yhat[:, 0], yhat[-1:, 1:]), axis=None)
        # inv_yhat = inv_predict_out[(inv_predict_out.shape[0] - self.n_hours):, (n_features - 1):]
        # if isscale:
        #     o_dataset = self.scaler.inverse_transform(dataset_scaling)
        # else:
        #     o_dataset = dataset_scaling
        # inv_test_y = o_dataset[(o_dataset.shape[0] - self.n_hours):, (n_features - 1):]
        # ==============================================================================================================
        self.save_img_predict_test(inv_test_y, inv_yhat, "pm25")


# ======================================================================================================================
# define input sequence
# tem_a = array(
#     [23.75, 25.51, 26.51, 26.57, 26.81, 26.06, 26.05, 25.77, 25.46, 25.01, 24.63, 24.13, 23.47, 23.37, 23.25, 23.34,
#      23.39, 22.99])
# hud_a = array(
#     [81.04, 74.78, 71.69, 72.23, 71.81, 76.18, 77.02, 79.1, 81.37, 83.25, 84.46, 86.91, 89.22, 89.37, 89.45, 89.1,
#      89.22, 89.65])
# pm_a = array(
#     [101.7, 73.96, 67.38, 58.03, 44.5, 46.58, 45.98, 49.74, 44.54, 41.12, 43.8, 47.18, 49.78, 53.67, 50.35, 47.92,
#      49.11, 84.41])
# n_steps_in, n_steps_out = 7, 3
#
# in_tem = tem_a.reshape((len(tem_a), 1))
# in_hud = hud_a.reshape((len(hud_a), 1))
# out_pm = pm_a.reshape((len(pm_a), 1))
# # horizontally stack columns
# dataset = np.hstack((in_tem, in_hud, out_pm))
# # choose a number of time steps
# dataset_traning = dataset[:len(dataset) - (n_steps_in + n_steps_out) + 1]
# dataset_test = dataset[len(dataset)-(n_steps_in + n_steps_out) + 1:len(dataset)]
# split into samples
# X_, y = split_sequences(dataset_traning, n_steps_in, n_steps_out)
# X_test, y_test = split_sequences(dataset_test, n_steps_in, n_steps_out)
# n_features = X_.shape[2]
# ======================================================================================================================
# ======================================================================================================================
file_path = []
file_path_1 = r'D:\MyApp\PRJ\tensorflow_RNN\data_csv\1.xlsx'
file_path_2 = r'D:\MyApp\PRJ\tensorflow_RNN\data_csv\2.xlsx'
file_path_3 = r'D:\MyApp\PRJ\tensorflow_RNN\data_csv\3.xlsx'
file_path_4 = r'D:\MyApp\PRJ\tensorflow_RNN\data_csv\4.xlsx'
file_path_5 = r'D:\MyApp\PRJ\tensorflow_RNN\data_csv\5.xlsx'
# file_path.append(file_path_1)
# file_path.append(file_path_2)
# file_path.append(file_path_3)
file_path.append(file_path_4)
# file_path.append(file_path_5)
sheet_temp = []
sheet_hum = []
sheet_aqi = []
for i in file_path:
    dfs = pd.read_excel(i, sheet_name=None)
    sheet_temp.append(dfs["Nhiệt độ"].values.tolist())
    sheet_hum.append(dfs["Độ ẩm"].values.tolist())
    sheet_aqi.append(dfs["AQI"].values.tolist())

temp_l = [item for sublist in sheet_temp for item in sublist]
hum_l = [item for sublist in sheet_hum for item in sublist]
aqi_l = [item for sublist in sheet_aqi for item in sublist]

temp_l_station1 = [row[223] for row in temp_l]
temp_l_station2 = [row[33] for row in temp_l]
temp_l_station3 = [row[50] for row in temp_l]

hum_l_station1 = [row[223] for row in hum_l]
hum_l_station2 = [row[33] for row in hum_l]
hum_l_station3 = [row[50] for row in hum_l]

aqi_l_station1 = [row[223] for row in aqi_l]
aqi_l_station2 = [row[33] for row in aqi_l]
aqi_l_station3 = [row[50] for row in aqi_l]

# array_last_np = np.column_stack((temp_l_station1, temp_l_station2, temp_l_station3, hum_l_station1, hum_l_station2,
#                                  hum_l_station3, aqi_l_station1, aqi_l_station2, aqi_l_station3))

array_last_np = np.column_stack(
    (temp_l_station2, temp_l_station3, hum_l_station2, hum_l_station3, aqi_l_station2, aqi_l_station3))

# ======================================================================================================================
notonly = True

units = 200
# n_hours = 3 * 24  # out
# n_time_predicts = 7 * 24
n_hours = 2  # out
n_time_predicts = 5
epochs = 1000
batch_size = None
verbose = 1
min_delta = 1e-30
patience = 30
# min_delta = None
# patience = None
monitor = 'val_loss'
coin = None
n_features = 6
# opt = adam(lr=0.00001)
opt = 'adam'
isscale = False
demo = True

config_train = (epochs, batch_size, verbose, min_delta, patience, monitor)
# noinspection PyTypeChecker
trainModel = trainModel(None, n_hours, n_time_predicts, config_train, units, opt)
# X_, y = trainModel.split_sequences(dataset_traning, n_steps_in, n_steps_out)


# horizontally stack columns
# if notonly:
#     dataset_ = np.hstack((in_tem_, in_hud_, out_pm_))
# else:
#     dataset_ = np.hstack(out_pm_)
dataset_ = array_last_np

# # choose a number of time steps
# dataset_traning_ = dataset_[:len(dataset_) - (n_time_predicts + n_hours)]
# dataset_test_ = dataset_[len(dataset_) - (n_time_predicts + n_hours):len(dataset_)]
# # split into samples
# X__, y_ = trainModel.split_sequence_one(dataset_traning_, n_time_predicts, n_hours)
# X_test_, y_test_ = trainModel.split_sequence_one(dataset_test_, n_time_predicts, n_hours)
# # n_features = X__.shape[2]
# X__ = X__.reshape((X__.shape[0], X__.shape[1], n_features))
# X_test_ = X_test_.reshape((X_test_.shape[0], X_test_.shape[1], n_features))
# endregion

# d, d2 = trainModel.modifire_dataset(dataset_, n_hours, n_features)

dataset__ = dataset_.reshape(dataset_.shape[0], n_features)
dataset__ = dataset__.astype('float32')
dataset__ = trainModel.series_to_supervised(dataset__, n_hours, n_time_predicts)
dataset__c = []
for n in range(0, n_features * (n_hours + n_time_predicts)):
    if (n % n_features != (n_features - 1)) and n >= n_features * n_time_predicts:
        # dataset__ = dataset__.drop(dataset__.columns[n], axis=1)
        dataset__c.append(dataset__.columns.values.tolist()[n])
for n in dataset__c:
    dataset__ = dataset__.drop(n, axis=1)
if isscale:
    dataset_scaling = trainModel.scaler.fit_transform(dataset__)
else:
    dataset_scaling = trainModel.scaler.inverse_transform(trainModel.scaler.fit_transform(dataset__))

# dataset, dataset2 = trainModel.modifire_dataset(dataset_scaling, n_hours, n_features)

train, valid = trainModel.split_train_test(np.asarray(dataset_scaling), trainModel.n_time_predicts)

train_X, train_y = trainModel.split_into_inputs_and_outputs(train, n_features=n_features,
                                                            n_time_predicts=n_time_predicts,
                                                            n_hours=n_hours)
valid_X, valid_y = trainModel.split_into_inputs_and_outputs(valid, n_features=n_features,
                                                            n_time_predicts=n_time_predicts,
                                                            n_hours=n_hours)

trainModel.train_model(train_X, train_y, valid_X, valid_y, dataset_scaling, n_features, isscale, demo)

# region 1
# # define model
# model = Sequential()
# # 100 neurons in the first hidden layer
# # features = n_features
# # 1 lop LSTM , co input = input shape
# model.add(LSTM(100, activation='relu', return_sequences=True, input_shape=(n_time_predicts, n_features)))
# model.add(Dropout(0.2))
# #  1 lop LSTM nua cung co 100 neuron
# model.add(LSTM(100, activation='relu'))
# # 1 layer dense de lay output co so luong la n_steps_out
# model.add(Dense(n_hours))
# # model.add(Dense(n_steps_out, activation='linear'))
# # opt = SGD(lr=0.001, momentum=
# # opt = adam(lr=0.00001, decay=0.01)
# model.summary()
# model.compile(optimizer='adam', loss='mse')
# # fit model1570924200000
# # 100 training epochs with a batch size of -----
# # history = model.fit(X__, y_, epochs=50, batch_size=72, verbose=2, validation_data=(X_test_, y_test_))
# history = model.fit(X__, y_, epochs=50, verbose=1, validation_data=(X_test_, y_test_))
# # plot history
# pyplot.plot(history.history['loss'], label='train')
# pyplot.plot(history.history['val_loss'], label='test')
# pyplot.legend()
# pyplot.show()
# # ======================================================================================================================
# # demonstrate prediction
# # 23.39, 22.99, 22.57
# # 89.22, 89.65, 90.25
# # x_input = X[n_steps_in:-1]
# # x_input = x_input.reshape((1, n_steps_in, n_features))
# yhat = model.predict(X_test_)
# pyplot.title("Forecast vs Actual", fontsize=14)
# pyplot.plot(pd.Series(np.ravel(y_test_[0])), color='violet', label="Actual0")
# pyplot.plot(pd.Series(np.ravel(yhat[0])), color='g', label="Forecast0")
# # pyplot.plot(pd.Series(np.ravel(y_test[1])), color='red', label="Actual1")
# # pyplot.plot(pd.Series(np.ravel(yhat[1])), color='orange', label="Forecast1")
# pyplot.legend(loc="upper left")
# pyplot.xlabel("Time Periods")
# pyplot.show()
# print("=================================")
# endregion
