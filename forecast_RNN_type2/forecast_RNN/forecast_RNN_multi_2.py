import csv
import json
import os
from pathlib import Path

from keras import Sequential
from keras.callbacks import EarlyStopping
from keras.engine.saving import model_from_json
from keras.layers import LSTM, Dropout, Dense
from keras.optimizers import SGD, adam
from pandas import read_csv, DataFrame, concat
import pandas as pd
import numpy as np
from numpy import array
from datetime import datetime
from matplotlib import pyplot
from sklearn.preprocessing import MinMaxScaler


# noinspection PyMethodMayBeStatic,PyShadowingNames
class trainModel(object):
    # split a multivariate sequence into samples
    # noinspection PyShadowingNames
    def __init__(self, coin=None, n_hours=1, n_time_predicts=1, config_train=None, units=None, opt='adam'):
        self.coin = coin
        self.n_hours = n_hours
        self.n_time_predicts = n_time_predicts
        self.config_train = config_train
        self.units = units
        self.opt = opt
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        # self.db = ConnectDB()
        # self.scaler = MinMaxScaler(feature_range=(0, 1))

    # noinspection PyMethodMayBeStatic,PyShadowingNames
    def split_sequence_one(self, sequence, n_steps_in, n_steps_out):
        X, y = list(), list()
        for i in range(len(sequence)):
            # find the end of this pattern
            end_ix = i + n_steps_in
            out_end_ix = end_ix + n_steps_out
            # check if we are beyond the sequence
            if out_end_ix > len(sequence):
                break
            # gather input and output parts of the pattern
            seq_x, seq_y = sequence[i:end_ix], sequence[end_ix:out_end_ix]
            X.append(seq_x)
            y.append(seq_y)
        return array(X), array(y)

    # split a multivariate sequence into samples
    def split_sequences(self, sequences, n_steps_in, n_steps_out):
        X, y = list(), list()
        for i in range(len(sequences)):
            # find the end of this pattern
            end_ix = i + n_steps_in
            out_end_ix = end_ix + n_steps_out - 1
            # check if we are beyond the dataset
            if out_end_ix > len(sequences):
                break
            # gather input and output parts of the pattern
            seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix - 1:out_end_ix, -1]
            X.append(seq_x)
            y.append(seq_y)
        return array(X), array(y)

    def series_to_supervised(self, data, n_out=1, n_in=1, dropnan=True):
        n_vars = 1 if type(data) is list else data.shape[1]
        df = DataFrame(data)
        df_drop = DataFrame(data)
        df_drop.drop(df_drop.columns[range(1, n_vars)], axis=1, inplace=True)
        cols, names = list(), list()
        for i in range(0, n_in):
            cols.append(df.shift(i))
            if i == 0:
                names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
            else:
                names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
        for i in range(1, n_out + 1):
            cols.append(df_drop.shift(-i))
            names += [('var%d(t+%d)' % (1, i))]
        agg = concat(cols, axis=1)
        agg.columns = names
        if dropnan:
            agg.dropna(inplace=True)
        agg = agg.fillna(0)
        return agg

    def normalize_data(self, dataset, dropnan=True):
        values = dataset.values
        values = values.astype('float32')
        reframed = self.series_to_supervised(values, self.n_hours, 1, dropnan)
        values = reframed.values
        return values

    def modifire_dataset(self, dataset, n_hours, n_features):
        X, Y = list(), list()
        for i in range(len(dataset)):
            if i >= len(dataset) - n_hours:
                continue
            start_i = i
            end_i = i + n_hours
            if n_features == 1:
                gr_data = dataset[start_i + 1:end_i + 1]
            else:
                gr_data = dataset[start_i + 1:end_i + 1, -1:]
            local_data = dataset[i]
            numpy_add = np.append(local_data, gr_data)
            X.append(numpy_add)
            Y.append(numpy_add.tolist())
        # X = array(X)
        # X = X.reshape((X.shape[0], X.shape[1], n_features))
        return X, Y

    def split_train_test(self, values, n_time_predicts, n_hours):
        # n_train_hours = len(values) - n_time_predicts
        # train = values[:n_train_hours, :]  # lấy hàng 0 đến hàng n_train_hours và tất cả các cột của mảng đa chiều values
        # test = values[n_train_hours:, :]  # lấy hàng n_train_hours đến hết  và tất cả các cột của mảng đa chiều values
        train, valid_test = np.split(values, [int(0.6 * len(values))])
        # valid, test = np.split(valid_test, [int(0.5 * len(valid_test))])
        valid = valid_test[:valid_test.shape[0] - n_time_predicts - n_hours, :]
        test = valid_test[valid_test.shape[0] - n_time_predicts - n_hours:, :]
        return train, valid, test

    def split_into_inputs_and_outputs(self, notonly, values, n_features=10):
        n_time_predicts = len(values)
        X, y = values[:, :n_features], values[:, n_features:]

        # if notonly:
        #     X = X.reshape((X.shape[0], X.shape[1], n_features))
        # else:
        #     X = X.reshape((n_time_predicts, 1, n_features))
        X = X.reshape((n_time_predicts, 1, n_features))
        return X, y

    def invert_scaling(self, yhat, test_X_reshape, part_2, n_features, isscale, demo):
        first_y = np.concatenate((yhat[:, 0], yhat[-1:, 1:]), axis=None)
        first_y_reshape = first_y.reshape(first_y.shape[0], 1)
        test_X_reshape_split = test_X_reshape[:, :(n_features - 1)]
        # new_data_forecast =
        # inv_y = np.concatenate((test_y, test_X_reshape[:, -(n_features - 1):]), axis=1)

        # inv_y = np.concatenate((first_y_reshape, test_X_reshape_split), axis=1)
        if demo:
            inv_y_part = np.concatenate((test_X_reshape_split, yhat[:, 0].reshape(yhat.shape[0], 1)), axis=1)
        else:
            inv_y_part = np.concatenate(
                (part_2[:, :(n_features - 1)], yhat[yhat.shape[0] - 1].reshape(self.n_hours, 1)),
                axis=1)
        inv_y_part_2 = yhat
        # inv_y = self.scaler.inverse_transform(inv_y)
        # inv_y = inv_y[:, 0]
        return inv_y_part, yhat

    def build_model(self, units, train_X, loss='mse', optimizer='adam'):
        model = Sequential()
        model.add(LSTM(units, input_shape=(train_X.shape[1], train_X.shape[2]), return_sequences=True))
        # activation='tanh' = default
        model.add(LSTM(units, return_sequences=True))
        model.add(LSTM(units))
        # model.add(LSTM(units))
        # model.add(LSTM(1, return_sequences=True))
        model.add(Dense(self.n_hours, activation='relu'))
        model.summary()
        model.compile(loss=loss, optimizer=optimizer, metrics=['acc'])
        return model

    # noinspection PyUnusedLocal
    def make_predict(self, model, test_X, test_y, part_2, n_time_predicts, n_features=1, isscale=True, demo=True):
        yhat = model.predict(test_X)
        test_X = test_X.reshape((test_X.shape[0], test_X.shape[1] * n_features))
        inv_yhat_part, yhat = self.invert_scaling(yhat, test_X, part_2, n_features, isscale, demo)
        return inv_yhat_part, yhat

    def save_img_predict_test(self, test_y, inv_yhat, symbol):
        print("=================================")
        pyplot.title("Chart", fontsize=14)
        pyplot.plot(test_y, color='violet', label='Actual')
        # pyplot.legend(loc="upper left")
        #         # pyplot.xlabel("Time Periods")
        #         # pyplot.show()
        #         # pyplot.title("Forecast", fontsize=14)
        pyplot.plot(inv_yhat, color='g', label='Forecast')
        pyplot.legend(loc="upper left")
        pyplot.xlabel("Time Periods")
        pyplot.draw()
        folder_save_png = os.path.join(ROOT_DIR, 'img', 'chart' + datetime.now().strftime("%m%d%Y%H%M%S") + '.png')
        pyplot.savefig(folder_save_png)
        pyplot.show()
        pyplot.close()

    def fit_model(self, model, train_X, train_y, test_X, test_y, config):
        epochs, batch_size, verbose, min_delta, patience, monitor = config
        # history = model.fit(train_X, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose, shuffle=False,
        #                     validation_data=(test_X, test_y),
        #                     callbacks=[EarlyStopping(monitor=monitor, min_delta=min_delta, patience=patience)])
        history = model.fit(train_X, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose, shuffle=False,
                            validation_data=(test_X, test_y))
        pyplot.plot(history.history['loss'], label='train')
        pyplot.plot(history.history['val_loss'], label='test')
        pyplot.legend()
        pyplot.draw()
        folder_save_png = os.path.join(ROOT_DIR, 'img', 'chart_loss2' + '.png')
        pyplot.savefig(folder_save_png)
        pyplot.show()
        pyplot.close()
        return model

    def save_model(self, model, symbol, config):
        folder_save_w = os.path.join(ROOT_DIR, 'weights', 'weight2' + '.h5')
        folder_save_json = os.path.join(ROOT_DIR, 'models', 'model2' + '.json')
        if config["renew_model"] == 1:
            model.save_weights(folder_save_w)
            model_json = model.to_json()
            # if config["renew_model"] == 1:
            with open(folder_save_json, "w") as json_file:
                json_file.write(model_json)

    # noinspection PyTypeChecker
    def train_model(self, train, valid, test, X, y, X_valid, y_valid, test_X, test_y, dataset_scaling, n_features,
                    isscale, demo, config):
        if config["renew_model"] == 1:
            model = self.build_model(units=self.units, train_X=X, optimizer=self.opt)
            model = self.fit_model(model, X, y, X_valid, y_valid, self.config_train)
            self.save_model(model, config["param_f"], config)
        else:
            # load json and create model
            folder_save_w = os.path.join(ROOT_DIR, 'weights', 'weight' + '.h5')
            folder_save_json = os.path.join(ROOT_DIR, 'models', 'model' + '.json')
            json_file_model = open(folder_save_json, 'r')
            loaded_model_json = json_file_model.read()
            json_file_model.close()
            model = model_from_json(loaded_model_json)
            # load weights into new model
            model.load_weights(folder_save_w)

        # other_part = dataset_scaling[(dataset_scaling.shape[0] - self.n_hours):, :(n_features - 1)]
        part_2 = dataset_scaling[(dataset_scaling.shape[0] - self.n_hours):, :]
        part_1 = dataset_scaling[:(dataset_scaling.shape[0] - self.n_hours), :]
        if demo:
            # ==============================================================================================================
            test_X_modifired = test_X[0: self.n_time_predicts]
            yhat_part, o_yhat = self.make_predict(model, test_X_modifired, test_y, None, self.n_time_predicts,
                                                  n_features,
                                                  isscale, True)
            # predict_out_all = np.concatenate((part_1, yhat_part))
            predict_out_all = yhat_part
            if isscale:
                inv_predict_out = self.scaler.inverse_transform(predict_out_all)
            else:
                inv_predict_out = predict_out_all
            # inv_yhat = inv_predict_out[:, (n_features - 1):]
            inv_yhat_tem = o_yhat[o_yhat.shape[0] - 1:o_yhat.shape[0], :]
            inv_yhat = inv_yhat_tem.reshape(inv_yhat_tem.shape[1], 1)
            if isscale:
                o_dataset = self.scaler.inverse_transform(test_X.reshape((test_X_modifired.shape[0], 1 * n_features)))
            else:
                o_dataset = dataset_scaling
            # inv_test_y = o_dataset[:, (n_features - 1):]
            # inv_test_y = test_y[:, :1]
            # inv_test_y = test_y[self.n_time_predicts:test_y.shape[0] - 1, :1]
            inv_test_y = test_y[self.n_time_predicts - 1:self.n_time_predicts + self.n_hours - 1, :1]
        else:
            pass
        # ==============================================================================================================
        # yhat_part = self.make_predict(model, X_test, part_2, n_features, isscale)
        # predict_out_all = np.concatenate((part_1, yhat_part))
        # if isscale:
        #     inv_predict_out = self.scaler.inverse_transform(predict_out_all)
        # else:
        #     inv_predict_out = predict_out_all
        # # inv_yhat = np.concatenate((yhat[:, 0], yhat[-1:, 1:]), axis=None)
        # inv_yhat = inv_predict_out[(inv_predict_out.shape[0] - self.n_hours):, (n_features - 1):]
        # if isscale:
        #     o_dataset = self.scaler.inverse_transform(dataset_scaling)
        # else:
        #     o_dataset = dataset_scaling
        # inv_test_y = o_dataset[(o_dataset.shape[0] - self.n_hours):, (n_features - 1):]
        # ==============================================================================================================
        self.save_img_predict_test(inv_test_y, inv_yhat, config["param_f"])  # cot 1 la forecast, cot 2 la real
        out_list = np.hstack((inv_test_y, inv_yhat)).tolist()
        out_csv = os.path.join(ROOT_DIR, 'data_csv', 'data_f2_' + datetime.now().strftime("%m%d%Y%H%M%S") + '.csv')
        with open(out_csv, "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerows(out_list)
        print("===")


# ======================================================================================================================

verbose = 1
min_delta = 1e-30
patience = 30
monitor = 'val_loss'
coin = None
# n_features = 3
# opt = 'adam'
# isscale = False
demo = True

config = {}
ROOT_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)))
config_path = os.path.join(ROOT_DIR, 'forecast_RNN', 'config' + '.json')

with open(config_path, 'r') as myfile:
    if len(myfile.readlines()) != 0:
        myfile.seek(0)
        config = json.load(myfile)

config_train = (config["epochs"], config["batch_size"], verbose, min_delta, patience, monitor)
if config["isscale"] == 1:
    isscale = True
else:
    isscale = False
# noinspection PyTypeChecker
trainModel = trainModel(None, config["n_hours"], config["n_time_predicts"], config_train, config["units"],
                        config["opt"])
# X_, y = trainModel.split_sequences(dataset_traning, n_steps_in, n_steps_out)

# ======================================================================================================================
file_path = os.path.join(ROOT_DIR, 'data_csv', 'data' + '.csv')
data = pd.read_csv(file_path, delimiter=',', header=0, skipinitialspace=True)
# data.head(24)

a_list_param = []
for i in config["param"]:
    a = np.array(data[i])
    a_list_param.append(a.reshape((len(a), 1)))

# tem_a = np.array(data['tem'])
# hud_a = np.array(data['hud'])
# pm_a = np.array(data['pm25'])

# convert to [rows, columns] structure
# in_tem_ = tem_a.reshape((len(tem_a), 1))
# in_hud_ = hud_a.reshape((len(hud_a), 1))
# out_pm_ = pm_a.reshape((len(pm_a), 1))

# ======================================================================================================================
notonly = True
dataset_ = None
if notonly:
    for i in a_list_param:
        # dataset_ = np.hstack((in_tem_, in_hud_, out_pm_))
        if dataset_ is None:
            dataset_ = i
        else:
            dataset_ = np.hstack((dataset_, i))
else:
    dataset_ = np.hstack(a_list_param[len(a_list_param) - 1])

dataset__ = dataset_.reshape(dataset_.shape[0], config["n_features"])
if isscale:
    dataset_scaling = trainModel.scaler.fit_transform(dataset__)
else:
    dataset_scaling = dataset__

dataset, dataset2 = trainModel.modifire_dataset(dataset_scaling, config["n_hours"], config["n_features"])

if config["renew_model"] == 1:
    train, valid, test = trainModel.split_train_test(np.asarray(dataset2), trainModel.n_time_predicts,
                                                     trainModel.n_hours)
    train_X, train_y = trainModel.split_into_inputs_and_outputs(notonly, train, n_features=config["n_features"])
    valid_X, valid_y = trainModel.split_into_inputs_and_outputs(notonly, valid, n_features=config["n_features"])
    test_X, test_y = trainModel.split_into_inputs_and_outputs(notonly, test, n_features=config["n_features"])
else:
    # valid = valid_test[:valid_test.shape[0] - n_time_predicts - n_hours, :]
    train, valid, test = None, None, np.asarray(dataset2)
    train_X, train_y = None, None
    valid_X, valid_y = None, None
    test_X, test_y = trainModel.split_into_inputs_and_outputs(notonly, test, n_features=config["n_features"])

trainModel.train_model(train, valid, test, train_X, train_y, valid_X, valid_y, test_X, test_y, dataset_scaling,
                       config["n_features"], isscale, demo, config)
