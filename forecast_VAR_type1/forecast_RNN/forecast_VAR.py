import csv
import datetime
import json
import os
import numpy
import pandas as pd
from matplotlib import pyplot
from statsmodels.tsa.vector_ar.var_model import VAR


# noinspection PyShadowingNames
def recursive_(init_step, predict_t, init_train, cols):
    if init_step > 0:
        model = VAR(endog=init_train)
        model_fit = model.fit()
        yhat = model_fit.forecast(model_fit.y, steps=1)
        init_step = init_step - 1
        init_train_v = init_train.values
        new_train = numpy.concatenate([init_train_v, yhat])
        df = pd.DataFrame(new_train, columns=cols)
        recursive_(init_step, predict_t, df, cols)
    else:
        o = init_train.values[len(init_train) - predict_t:, 0:1]
        return o


config = {}
ROOT_DIR = os.path.dirname(os.path.dirname(__file__))
config_path = os.path.join(ROOT_DIR, 'forecast_RNN', 'config' + '.json')
with open(config_path, 'r') as myfile:
    if len(myfile.readlines()) != 0:
        myfile.seek(0)
        config = json.load(myfile)
# ======================================================================================================================
weather_v_filepath = os.path.join(ROOT_DIR, 'data_csv', 'darkinfohourly' + '.csv')
csv_reader = pd.read_csv(weather_v_filepath)
out_w = csv_reader.filter(["timezone", "data.time", "data.summary", "data.windSpeed", "data.windBearing", "code", "longtime"])
out_w = out_w[out_w['code'] == 2]
# ======================================================================================================================
weather_l = []
weather_path = os.path.join(ROOT_DIR, 'package_func', 'weather_def' + '.json')
weather_def_l = []
with open(weather_path, 'r', encoding='utf-8') as myfile:
    if len(myfile.readlines()) != 0:
        myfile.seek(0)
        weather_def_l = json.load(myfile)
for x in out_w.values:
    mem_w = []
    mem_w_ws = x[3]
    mem_w_wb = x[4]
    mem_w_time = datetime.datetime.fromtimestamp(x[6]).strftime('%d/%m/%Y %H:%M:%S')
    mem_w_h = datetime.datetime.fromtimestamp(x[6]).hour
    mem_w_w = next((y for y in weather_def_l if y['en'] == x[2]), None)['index']
    mem_w.append(mem_w_time)
    mem_w.append(mem_w_h)
    mem_w.append(mem_w_w)
    mem_w.append(mem_w_ws)
    mem_w.append(mem_w_wb)
    weather_l.append(mem_w)
# ======================================================================================================================
verbose = 1
min_delta = 1e-30
patience = 30
monitor = 'val_loss'
coin = None
demo = True

# config_train = (config["epochs"], config["batch_size"], verbose, min_delta, patience, monitor)
if config["isscale"] == 1:
    isscale = True
else:
    isscale = False
# ======================================================================================================================
files_l = []
for files in config["files"]:
    file_path = os.path.join(ROOT_DIR, 'data_csv', files)
    # data = pd.read_csv(file_path, delimiter=',', header=0, skipinitialspace=True)
    dfs = pd.read_excel(file_path, sheet_name=None)
    dfs_numpy = dfs['data']
    files_l.append(dfs_numpy.values.tolist())

# data.head(24)
index_time = []
for i in files_l:
    for j in i:
        index_time.append(j[0])
for i in weather_l:
    index_time.append(i[0])
index_time_set = list(set(index_time))
index_time_s = sorted(index_time_set, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y %H:%M:%S'))
merge_l = []
for i in index_time_s:
    mem = []
    dt = datetime.datetime.strptime(i, '%d/%m/%Y %H:%M:%S').hour
    mem.append(i)
    mem.append(dt)
    weather_c = next((y for y in weather_l if y[0] == i), None)
    if weather_c is not None:
        mem.append(weather_c[2])
        mem.append(weather_c[3])
        mem.append(weather_c[4])
    else:
        mem.append("nan")
    for f1 in files_l:
        first_or_default = next((x for x in f1 if x[0] == i), None)
        if first_or_default is not None:
            mem.append(first_or_default[1])
        else:
            mem.append("nan")
    if "nan" not in mem:
        merge_l.append(mem)
# ======================================================================================================================
notonly = True
# horizontally stack columns
dataset_ = None
if notonly:
    a = numpy.array(merge_l)
    dataset_ = a[:, 1:config["n_features"] + 2].astype(numpy.float)
else:
    # dataset_ = np.hstack(a_list_param[len(a_list_param) - 1])
    pass


#
# # check the dtypes
# df['Date_Time'] = pd.to_datetime(df.Date_Time, format='%d/%m/%Y %H.%M.%S')
# data = df.drop(['Date_Time'], axis=1)
# # data.index = df.Date_Time
# data = data.drop([data.columns[13], data.columns[14]], axis=1)
# # missing value treatment
# cols = data.columns
# for j in cols:
#     for i in range(0, len(data)):
#         if data[j][i] == -200:
#             data[j][i] = data[j][i - 1]
#
# johan_test_temp = data.drop(['CO(GT)'], axis=1)
# coint_johan = coint_johansen(johan_test_temp, -1, 1).eig

train = dataset_[:len(dataset_) - config["n_hours"]]
test = dataset_[len(dataset_) - config["n_hours"]:]

model = VAR(endog=train)
model_fit = model.fit()
# make prediction on validation
yhat = model_fit.forecast(model_fit.y, steps=config["n_hours"])

# yhat = recursive_(predict_t, predict_t, train, cols)
pyplot.title("Chart", fontsize=14)
# pyplot.plot(test.values[:, 0:1], color='violet', label='Actual')
pyplot.plot(test[:, -1:], color='violet', label='Actual')
# pyplot.plot(yhat[:, 0:1], color='g', label='Forecast')
pyplot.plot(yhat[:, -1:], color='g', label='Forecast')
pyplot.legend(loc="upper left")
pyplot.xlabel("Time Periods")
pyplot.draw()
folder_save_png = os.path.join(ROOT_DIR, 'img', 'chartVAR_' + datetime.datetime.now().strftime("%m%d%Y%H%M%S") + '.png')
pyplot.savefig(folder_save_png)
pyplot.show()
pyplot.close()

# out_list = numpy.np.hstack((test.values[:, 0:1], yhat[:, 0:1])).tolist()
out_list = numpy.hstack((test[:, -1:], yhat[:, -1:])).tolist()
out_csv = os.path.join(ROOT_DIR, 'data_csv', 'data_VAR_' + datetime.datetime.now().strftime("%m%d%Y%H%M%S") + '.csv')
with open(out_csv, "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(out_list)

print("""""""")
