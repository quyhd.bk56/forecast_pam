Đối với thuật toán dùng VAR:
- Thư viện sử dụng : statsmodel (https://www.statsmodels.org/stable/index.html)
- Các trang tham khảo: 
    + https://towardsdatascience.com/granger-causality-and-vector-auto-regressive-model-for-time-series-forecasting-3226a64889a6
    + https://machinelearningmastery.com/time-series-forecasting-methods-in-python-cheat-sheet/
    + https://towardsdatascience.com/forecasting-with-technical-indicators-and-gru-lstm-rnn-multivariate-time-series-a3244dcbc38b

Đối với thuật toán dùng LSTM:
- Thư viện sử dụng : keras
- Các trang tham khảo:
    + https://machinelearningmastery.com/tutorial-first-neural-network-python-keras/
    + https://viblo.asia/p/deploy-ung-dung-machine-learning-len-web-server-phan-6-du-bao-da-buoc-da-bien-voi-lstm-keras-python-maGK7mRDlj2
    + https://viblo.asia/p/deep-learning-qua-kho-dung-lo-da-co-keras-LzD5dBqoZjY
    + https://machinelearningmastery.com/how-to-develop-lstm-models-for-time-series-forecasting/
    + https://machinelearningmastery.com/multivariate-time-series-forecasting-lstms-keras/
    + https://machinelearningmastery.com/multi-step-time-series-forecasting-long-short-term-memory-networks-python/
    + https://machinelearningmastery.com/learning-curves-for-diagnosing-machine-learning-model-performance/
    + https://blog.quantinsti.com/rnn-lstm-gru-trading/?utm_campaign=News&utm_medium=Community&utm_source=DataCamp.com